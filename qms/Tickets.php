<?php

namespace App\Controllers;

use App\Models\ServicesModel;
use App\Models\TicketsModel;
use CodeIgniter\API\ResponseTrait;
use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;


class Tickets extends BaseController
{
    use ResponseTrait;

    public function create()
    {
        $ipprint = getenv('IPPRINT');
        $serviceModel = new ServicesModel();
        $ticketModel = new TicketsModel();

        $serviceId = $this->request->getJSON(true)['service_id'];

        // cek apakah service ID yang diminta ada dalam database
        if (!$serviceModel->isServiceExist($serviceId)) {
            return $this->failNotFound('Layanan tidak ditemukan');
        }

        // get code service
        $service = $serviceModel->where('id', $serviceId)->first();

        // cek watu ambil ticker
        if (date('H:i:s') < $service['start_time']) {
            return $this->failForbidden('Belum waktu mulai ambil antrean');
        }

        if (date('H:i:s') > $service['end_time']) {
            return $this->failForbidden('Waktu ambil antrean sudah berakhir');
        }

        // dapatkan nomor antrian berikutnya untuk layanan ini
        $queueNumber = $ticketModel->getNextQueueNumber($serviceId);
        $sisaAntrean = $ticketModel->getCountQueue((array)$serviceId);


        // buat record ticket baru
        $ticket = [
            'service_id' => $serviceId,
            'queue_number' => $queueNumber,
            'code' => $service['code'],
            'priority' => $service['priority'],
            'created_at' => date('Y-m-d H:i:s')
        ];

        $ticketId = $ticketModel->insert($ticket);


        try {
            $connector = new NetworkPrintConnector($ipprint, 9100);
            $printer = new Printer($connector);
            $printer->setJustification(Printer::JUSTIFY_CENTER);
            $printer->text('RSUD RA. BASOENI MOJOKERTO');
            $printer->feed(1);
            $printer->text('Nomor Antrean');
            $printer->feed(2);
            $printer->setTextSize(6, 6);
            $printer->text($service['code'] . $queueNumber);
            $printer->feed(1);
            $printer->setTextSize(1, 1);
            $printer->text('Sisa antrean : ' . $sisaAntrean);
            $printer->feed(2);
            $printer->text('Jam ambil antrean : ' . $ticket['created_at']);
            $printer->feed(1);
            $printer->text('Simpan nomor antrean dan serahkan ke petugas');
            $printer->feed(1);
            $printer->cut();
        } catch (\Exception $e) {
            // Tangani kesalahan printer di sini, contoh:
            // Kirim respons gagal atau lakukan tindakan sesuai kebutuhan
            return $this->fail('Tidak dapat menghubungi Printer');
        } finally {
            $printer->close();
        }

        return $this->respondCreated([
            'ticketId' => $ticketId,
            'queueNumber' => $queueNumber,
            'queueLeft' => $sisaAntrean
        ]);
    }

    public function print($id)
    {
        $ticketModel = new TicketsModel();

        $ticket = $ticketModel->where('id', $id)->first();

        echo view('print_ticket', $ticket);
    }
}
