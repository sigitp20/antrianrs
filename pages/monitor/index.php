
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Display TV</title>
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="../../assets/css/adminlte.min.css">
  <script src="http://192.168.4.10//plugins/jquery/jquery.min.js"></script>
  <link rel="stylesheet" src="http://192.168.4.10//plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.css">

  <!-- <script src="../../assets/vendor/js/bootstrap.min.js" type="text/javascript"></script> -->


  <style>
    @media screen and (min-width: 1600px) {

      /* Atur tata letak, ukuran font, atau elemen lain sesuai kebutuhan untuk layar 1600px dan di atasnya */
      body {
        transform: scale(0.6);
        transform-origin: top left;
        width: 166.66667%;
        /* (1 / 0.6) */
        height: 166.66667%;
        overflow: hidden;
      }
    }

    @media screen and (min-width: 1920px) {

      /* Atur tata letak, ukuran font, atau elemen lain sesuai kebutuhan untuk layar 1600px dan di atasnya */
      body {
        transform: scale(0.8);
        transform-origin: top left;
        width: 166.66667%;
        /* (1 / 0.6) */
        height: 166.66667%;
        overflow: hidden;
      }
    }

    body {
      font-family: 'Roboto', sans-serif;
      background-color: #f5f5f5;
    }

    .navbar {
      background-color: #1a237e;
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
      border-radius: 0;
      padding: 10px;
    }

    .navbar-brand img {
      max-height: 60px;
      margin-top: -10px;
    }

    .navbar-brand .brand-text {
      font-size: 2.5rem;
      margin-left: 20px;
    }

    .navbar .text-white {
      font-size: 2rem;
    }

    .card {
      border-radius: 20px;
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
      overflow: hidden;
    }

    .card-header {
      background-color: #1565c0;
      color: #fff;
      font-size: 3rem;
      padding: 20px;
    }

    .card-body {
      font-size: 7rem;
      text-align: center;
      padding: 50px;
    }

    .card-footer {
      font-size: 2.5rem;
      text-align: center;
      background-color: #1a237e;
      color: #fff;
      padding: 20px;
    }

    .bg-gradient-success {
      background-image: linear-gradient(180deg, #007bff 10%, #28a745 90%);
    }

    .panel-video {
      height: 720px;
      overflow: hidden;
      border-radius: 20px;
      box-shadow: 0 2px 10px rgba(0, 0, 0, 0.3);
    }

    .embed-responsive {
      height: 100%;
    }

    .display-2 {
      font-size: 25rem;
      font-weight: bold;
      text-shadow: 2px 2px #1a237e;
    }

    @media (max-width: 991.98px) {
      .navbar .text-white {
        font-size: 1.5rem;
      }

      .navbar-brand .brand-text {
        font-size: 2rem;
      }

      .card-header {
        font-size: 2rem;
        padding: 10px;
      }

      .card-footer {
        font-size: 1.5rem;
        padding: 10px;
      }

      .display-2 {
        font-size: 15rem;
      }
    }

    @media (max-width: 767.98px) {
      .navbar .text-white {
        font-size: 1.2rem;
      }

      .navbar-brand .brand-text {
        font-size: 1.5rem;
        margin-left: 10px;
      }

      .card-header {
        font-size: 1.5rem;
        padding: 5px;
      }

      .card-footer {
        font-size: 1.2rem;
        padding: 5px;
      }

      .display-2 {
        font-size: 10rem;
      }
    }
  </style>
</head>


<?php
$hariIni = new DateTime();
function hariIndo($hariInggris)
{
    switch ($hariInggris) {
        case 'Sunday':
            return 'Minggu';
        case 'Monday':
            return 'Senin';
        case 'Tuesday':
            return 'Selasa';
        case 'Wednesday':
            return 'Rabu';
        case 'Thursday':
            return 'Kamis';
        case 'Friday':
            return 'Jumat';
        case 'Saturday':
            return 'Sabtu';
        default:
            return 'hari tidak valid';
    }
}

require_once "../../config/database.php";
$query = mysqli_query($mysqli, "SELECT * FROM queue_setting ORDER BY id DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
// ambil jumlah baris data hasil query
$rows = mysqli_num_rows($query);

if ($rows <> 0) {
    $data = mysqli_fetch_assoc($query);
} else {
    $data = [];
}
?>

<body class="layout-top-nav">
  <div class="wrapper">
    <nav class="main-header navbar navbar-expand-bd border-bottom navbar-dark bg-primary">
      <div class="container">
        <a href="#" style="align-items: left; justify-content: left;" class="navbar-brand" >
          <img src="../../assets/img/basoeni_logo.png" alt="RS Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">RSUD R.A Basoeni</span>
        </a>
        <!-- <div class="ml-auto text-white" id="datetime"></div> -->
        <div class="ml-auto text-white" id="date"><?= hariIndo(date('l')) . " " . strftime('%d %B %Y', $hariIni->getTimestamp()); ?></div>
        <div class="ml-auto text-white" id="time"></div>
      </div>
      <!-- Right navbar links -->

    </nav>
    <div class="content-wrapper">
      <div class="row">
        <div class="col-6">
          <div class="card bg-gradient-success">
            <div class="card-header">
              <h3 class="text-center">NOMOR ANTREAN</h3>
            </div>
            <div class="card-body">
              <!-- <h3 id="txtAntreanCall" class="display-2 text-center"></h3> -->
              <h3 id="antrian-sekarang" class="display-2 text-center"></h3>
            </div>
            <div class="card-footer">
              <h3 id="txtCounterCall" class="text-center namaLoketMonitor"></h3>
            </div>
          </div>
        </div>
        <div class="col-5">
          <div class="panel-video">
            <div class="embed-responsive embed-responsive-16by9">
              <video autoplay muted loop>
                <source src="../../assets/video/video.mp4" type="video/mp4">
              </video>
            </div>
          </div>
        </div>
      </div>
      <div id="carouselExampleIndicators" data-interval="3000" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
                        <div class="carousel-item active">
                <div class="row">
                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter1" class="text-center">LOKET 1 / NON RACIKAN</h3>
                          </div>
                          <div class="card-body">
                            <h1 id="txtCounterQueue1" style="font-size:14rem" class="text-center">B44</h3>
                          </div>
                        </div>
                      </div>

                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter2" class="text-center">LOKET 2 / RACIKAN</h3>
                          </div>
                          <div class="card-body">
                            <h1 id="txtCounterQueue2" style="font-size:14rem" class="text-center">A64</h3>
                          </div>
                        </div>
                      </div>

                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter3" class="text-center">LOKET 3</h3>
                          </div>
                          <div class="card-body">
                            <h1 id="txtCounterQueue3" style="font-size:14rem" class="text-center">A188</h3>
                          </div>
                        </div>
                      </div>

                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter4" class="text-center">LOKET 4</h3>
                          </div>
                          <div class="card-body">
                            <h1 id="txtCounterQueue4" style="font-size:14rem" class="text-center">A155</h3>
                          </div>
                        </div>
                      </div>

                                  </div>
              </div>
                          <div class="carousel-item">
                <div class="row">
                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter5" class="text-center">LOKET 5</h3>
                          </div>
                          <div class="card-body">
                            <h3 id="txtCounterQueue5" style="font-size:21rem" class="text-center"></h3>
                          </div>
                        </div>
                      </div>

                                        <div class="col-3">
                        <div class="card bg-gradient-success">
                          <div class="card-header">
                            <h3 id="txtCounter6" class="text-center">LOKET 6</h3>
                          </div>
                          <div class="card-body">
                            <h3 id="txtCounterQueue6" style="font-size:21rem" class="text-center"></h3>
                          </div>
                        </div>
                      </div>

                                  </div>
              </div>
                  </div>
      </div>

    </div>
  </div>

  <!-- load file audio bell antrian -->
  <audio id="tingtung" src="../../assets/audio/tingtung.mp3"></audio>

  <!-- Jquery dan Bootsrap JS -->
  <script src="../../assets/js/bootstrap.bundle.min.js"></script>
  <!-- <script src="http://192.168.4.10//dist/js/adminlte.min.js"></script> -->
  <script src="../../assets/js/adminlte.min.js"></script>
  <!-- <script src="http://192.168.4.10//plugins/sweetalert2/sweetalert2.all.js"></script> -->
  <script src="../../assets/js/sweetalert2.all.js"></script>

  
    <!-- Get API Key -> https://responsivevoice.org/ -->
    <script src="../../assets/vendor/js/responsivevoice.js" type="text/javascript"></script>

  <script>
    $(document).ready(function() {
      let queue = [];
      let isPlaying = false;
      let currentCallerId = null;

      let socket = null;
    //   const connectionUrl = 'ws://192.168.4.10:8000?connection=display';

    //   function connectWebSocket() {
    //     socket = new WebSocket(connectionUrl);

    //     socket.addEventListener('open', function(event) {
    //       Swal.close();
    //       console.log('WebSocket connection opened:', event);
    //       // Reset audio playback state and socket connection

    //       queue = [];
    //       isPlaying = false;
    //       currentCallerId = null;
    //     });

    //     socket.addEventListener('message', function(event) {
    //       console.log('Received message:', event.data);

    //       const message = JSON.parse(event.data);
    //       if (message.message === 'play_audio') {
    //         let audioList = [];
    //         audioList.push('http://192.168.4.10/audio/soundx.mp3'); // Bell
    //         audioList.push('http://192.168.4.10/audio/nomorantrian.mp3'); // Nomorantrian
    //         audioList.push(`http://192.168.4.10/audio/${message.data.queue_code}.mp3`); // Queue Code
    //         audioList.push(`http://192.168.4.10/audio/${message.data.queue_number}.mp3`); // Queue Code
    //         // message.data.queue_number.forEach(element => {
    //         //   audioList.push(`http://192.168.4.10/audio/${element}.mp3`); // tambahkan pesan ke dalam antrian
    //         // });
    //         audioList.push('http://192.168.4.10/audio/menuju.mp3');
    //         audioList.push('http://192.168.4.10/audio/loket.mp3');
    //         audioList.push(`http://192.168.4.10/audio/${message.data.caller}.mp3`); // Queue Code

    //         // message.data.caller.forEach(element => {
    //         //   audioList.push(`http://192.168.4.10/audio/${element}.mp3`); // tambahkan pesan ke dalam antrian
    //         // });

    //         enqueueAudio(audioList, message.caller_id, message.data.counter_id, message.data.counter_name, message.data.queue_str);
    //       }
    //     });

    //     socket.addEventListener('close', function(event) {
    //       console.log('WebSocket connection closed:', event);
    //       socket.close()
    //       Swal.fire('Websocket Server Disconnected');
    //       // Cobalah untuk menghubungkan kembali setelah koneksi ditutup
    //       setTimeout(connectWebSocket, 5000); // Coba koneksi ulang setelah 5 detik
    //     });

    //     socket.addEventListener('error', function(event) {
    //       console.error('WebSocket error occurred:', event);
    //       Swal.fire('Gagal Terhubung ke Websocket Server');
    //       // Cobalah untuk menghubungkan kembali jika terjadi kesalahan
    //       // setTimeout(location.reload(), 5000); // Coba koneksi ulang setelah 5 detik
    //     });
    //   }

      // Fungsi untuk mencoba koneksi pertama kali
    //   connectWebSocket();

    //   function playAudio() {
    //     // Jika antrian putar kosong atau sedang memutar audio, keluar dari fungsi
    //     if (queue.length === 0 || isPlaying) {
    //       return;
    //     }
    //     // Ambil antrian putar pertama
    //     const queueItem = queue[0];
    //     // Ubah status pemutaran audio menjadi true
    //     isPlaying = true;
    //     // Ambil audio pertama dari antrian putar
    //     const audio = queueItem.audioList.shift();
    //     // Mulai pemutaran audio
    //     audio.play();

    //     //Update Display
    //     $("#txtAntreanCall").html(queueItem.queue_str)
    //     $('#txtCounterCall').html(queueItem.counter_name.toUpperCase())
    //     $(`#txtCounterQueue${queueItem.counter_id}`).html(queueItem.queue_str);
    //     // Setelah audio selesai diputar, panggil fungsi playAudio untuk memutar audio berikutnya
    //     audio.onended = function() {
    //       isPlaying = false;
    //       // Jika tidak ada lagi audio dalam antrian putar saat ini, hapus antrian putar dan kirim pesan audio_finish ke websocket
    //       if (queueItem.audioList.length === 0) {
    //         queue.shift();
    //         let message = {
    //           message: "play_success",
    //           caller_id: queueItem.caller_id
    //         };
    //         socket.send(JSON.stringify(message));
    //       }
    //       // Panggil fungsi playAudio untuk memutar audio antrian putar selanjutnya
    //       playAudio();
    //     }
    //   }

      // Fungsi untuk menambah audio ke antrian putar dan memulai pemutaran audio jika belum ada audio yang sedang diputar
    //   function enqueueAudio(data, caller_id, counter_id, counter_name, queue_str) {
    //     // Buat audio dari setiap file audio pada data
    //     let audioList = [];
    //     for (let i = 0; i < data.length; i++) {
    //       let audio = new Audio(data[i]);
    //       audioList.push(audio);
    //     }
    //     // Tambahkan audio ke antrian putar
    //     queue.push({
    //       audioList: audioList,
    //       caller_id: caller_id,
    //       counter_id: counter_id,
    //       counter_name: counter_name,
    //       queue_str: queue_str
    //     });
    //     // Jika tidak ada audio yang sedang diputar, mulai pemutaran audio
    //     if (!isPlaying) {
    //       currentCallerId = caller_id;
    //       playAudio();
    //     }
    //   }

    //   function updateDateTime() {
    //     var now = new Date();
    //     var datetime = document.getElementById('datetime');
    //     datetime.innerHTML = now.toLocaleString();
    //   }

      // update setiap detik
    //   setInterval(updateDateTime, 1000);


    // buat variabel untuk menampilkan audio bell antrian
            var bell = document.getElementById('tingtung');
            var queuePanggil = [];
            var currentPanggil = 0;
            var isPlay = false;

            // var loket = localStorage.getItem('_loket');
            // $(".namaLoket").html(' Loket ' + loket);
            // var jenis_layanan = "";

            const checkQueuePanggil = (key, arrayOfQueue) => {
                var result = false;
                for (let i = 0; i < arrayOfQueue.length; i++) {
                    if (arrayOfQueue[i].id === key) {
                        result = true;
                    }
                }

                return result;
            }

            const get_panggilan = () => {
                $.ajax({
                    url: 'get_panggilan.php',
                    method: 'POST',
                    async: false,
                    cache: false,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result);
                        if (result.success == true) {
                            if (result.data.length > 0) {
                                result.data.forEach(function(element, index) {
                                    if (checkQueuePanggil(element.id, queuePanggil)) {
                                        return;
                                    }
                                    queuePanggil.push(element);
                                    if (!isPlay) {
                                        panggilAntrian();
                                    }
                                });
                            }
                        }
                    }
                });
            }

            const delete_panggilan = (id) => {
                $.ajax({
                    url: 'delete_panggilan.php',
                    method: 'POST',
                    data: {
                        id: id
                    },
                    async: false,
                    cache: false,
                    dataType: 'json',
                    success: function(result) {
                        console.log(result.message);
                    }
                });
            }

            $('#jumlah-antrian').load('../panggilan/get_jumlah_antrian.php');
            
            if ($('#antrian-sekarang').html().length == 4){
                $('#antrian-sekarang').load('../panggilan/get_antrian_sekarang.php');
            } 
            
            // $.ajax({
            //                 type: 'POST', // mengirim data dengan method POST
            //                 url: '../panggilan/get_antrian_sekarang.php', // insert.php url file proses insert data
            //                 data: {jenis_layanan: jenis_layanan},
            //                 success: function(result) { // ketika proses insert data selesai
            //                     $("#antrian-sekarang").html(result);
            //                 },
            //                 error: function (request, error) {
            //                     console.log(arguments);
            //                     alert(" Can't do because: " + error);
            //                 },
            //             });

            $('#antrian-selanjutnya').load('../panggilan/get_antrian_selanjutnya.php');
            get_panggilan();

            // auto reload data antrian setiap 1 detik untuk menampilkan data secara realtime
            setInterval(function() {
                $('#jumlah-antrian').load('../panggilan/get_jumlah_antrian.php').fadeIn("slow");
                // $('#antrian-sekarang').load('../panggilan/get_antrian_sekarang.php').fadeIn(1000);
                $("#antrian-sekarang").fadeOut(800);
                $("#antrian-sekarang").fadeIn(800);
                $('#antrian-selanjutnya').load('../panggilan/get_antrian_selanjutnya.php').fadeIn("slow");
                get_panggilan();

                // txtCounterQueue1 loket 1
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: '../panggilan/get_antrian_loket.php', // get_antrian_sekarang.php url file proses insert data
                            data: {loket: 1},
                            success: function(result) { // ketika proses insert data selesai
                                $("#txtCounterQueue1").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                        // txtCounterQueue2 loket 2
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: '../panggilan/get_antrian_loket.php', // get_antrian_sekarang.php url file proses insert data
                            data: {loket: 2},
                            success: function(result) { // ketika proses insert data selesai
                                $("#txtCounterQueue2").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                        // txtCounterQueue3 loket 3
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: '../panggilan/get_antrian_loket.php', // get_antrian_sekarang.php url file proses insert data
                            data: {loket: 3},
                            success: function(result) { // ketika proses insert data selesai
                                $("#txtCounterQueue3").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                        // txtCounterQueue4 loket 4
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: '../panggilan/get_antrian_loket.php', // get_antrian_sekarang.php url file proses insert data
                            data: {loket: 4},
                            success: function(result) { // ketika proses insert data selesai
                                $("#txtCounterQueue4").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                        // txtCounterQueue5 loket 5
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: '../panggilan/get_antrian_loket.php', // get_antrian_sekarang.php url file proses insert data
                            data: {loket: 5},
                            success: function(result) { // ketika proses insert data selesai
                                $("#txtCounterQueue5").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });


            }, 1000);

            function panggilAntrian() {
                if (queuePanggil.length > 0) {
                    queuePanggil.forEach((value, index) => {
                        if (!isPlay) {
                            isPlay = true;
                            $("#antrian-sekarang").html(value.code+value.antrian);
                            $(".namaLoketMonitor").html("LOKET " + value.loket);
                            // mainkan suara bell antrian
                            bell.currentTime = 0;
                            bell.pause();
                            bell.play();

                            // set delay antara suara bell dengan suara nomor antrian
                            durasi_bell = bell.duration * 770;

                            
                            var antrianpendek = 0;
                            if (value.antrian == 001 || value.antrian == 002 || value.antrian == 003 || value.antrian == 004 || value.antrian == 005 || value.antrian == 006 || value.antrian == 007 || value.antrian == 008 || value.antrian == 009 ){
                              antrianpendek = value.antrian.substring(2, 3);
                            } else if (value.antrian >= 010 && value.antrian <= 099){
                              antrianpendek = value.antrian.substring(1, 3);
                            } else if (value.antrian >= 100){
                              antrianpendek = value.antrian;
                            }

                            // mainkan suara nomor antrian
                            // responsiveVoice.speak("Nomor Antrian, " + value.code+" ,  "+value.antrian + ", menuju, loket, " + value.loket, "Indonesian Female", {
                            setTimeout(function() {
                                responsiveVoice.speak("Nomor Antrian, " + value.code+" ,  "+antrianpendek + ", menuju, loket, " + value.loket, "Indonesian Female", {
                                    rate: 0.9,
                                    pitch: 1,
                                    volume: 10,
                                    onend: () => {
                                        queuePanggil.splice(index, 1);
                                        isPlay = false;
                                        delete_panggilan(value.id);
                                        if (queuePanggil.length > 0) {
                                            panggilAntrian();
                                        }
                                    }
                                });
                            }, durasi_bell);
                        }
                    });
                }
            }
        });
    // })
  </script>

    <script>
        jam();

        function jam() {
            var e = document.getElementById("time"),
                d = new Date(),
                h,
                m,
                s;
            h = d.getHours();
            m = set(d.getMinutes());
            s = set(d.getSeconds());

            e.innerHTML = h + ":" + m + ":" + s;

            setTimeout("jam()", 1000);
        }

        function set(e) {
            e = e < 10 ? "0" + e : e;
            return e;
        }
    </script>

</body>

</html>