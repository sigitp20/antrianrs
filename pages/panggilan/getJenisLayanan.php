<?php
// Mengatasi CORS
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Credentials: true');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, x-requested-with, Content-Type, Accept, Access-Control-Request-Method");
header('Access-Control-Allow-Methods: GET, POST');
header("Allow: GET, POST");
// require 'cetak.php';
// require 'cetak333.php';
// pengecekan ajax request untuk mencegah direct access file, agar file tidak bisa diakses secara langsung dari browser
// jika ada ajax request
if (isset($_SERVER['REQUEST_METHOD']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
    // panggil file "database.php" untuk koneksi ke database
    require_once "../../config/database.php";

    // ambil tanggal sekarang
    $tanggal = gmdate("Y-m-d", time() + 60 * 60 * 7);
    $loket = $_POST['loket'];
    // var_dump($tipe);
    $query = mysqli_query($mysqli, "SELECT * FROM queue_setting ORDER BY id DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // ambil jumlah baris data hasil query
    $rows = mysqli_num_rows($query);

    if ($rows <> 0) {
        $data = mysqli_fetch_assoc($query);
    } else {
        $data = [];
    }

     $list_loket = json_decode($data['list_loket'], true);
     if (count($list_loket) > 0) :
    //  if ($list_loket) !== null) :
         foreach ($list_loket as $lk) : 
            if($lk['no_loket'] == $loket){
                echo $lk['jenis_layanan'];
            } else {
                // echo "0";
            }
        endforeach; 
     endif;

}
