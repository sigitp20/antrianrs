<?php

header("Allow: GET, POST"); 

// pengecekan ajax request untuk mencegah direct access file, agar file tidak bisa diakses secara langsung dari browser
// jika ada ajax request
if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest')) {
    // panggil file "database.php" untuk koneksi ke database
    require_once "../../config/database.php";

    // ambil tanggal sekarang
    $tanggal = gmdate("Y-m-d", time() + 60 * 60 * 7);
    $jenis_layanan = $_POST['jenis_layanan'];

    // // cek jika ada status 3 
    // $query22 = mysqli_query($mysqli, "SELECT no_antrian FROM queue_antrian_admisi  WHERE  tanggal='$tanggal' AND status='3' and code='$jenis_layanan' ORDER BY updated_date DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // // ambil jumlah baris data hasil query
    // $rows22 = mysqli_num_rows($query22);

    // if ($rows22 <> 0) {
    //     // ambil data hasil query
    //     $data = mysqli_fetch_assoc($query22);
    //     // buat variabel untuk menampilkan data
    //     $no_antrian22 = $data['no_antrian'];

    //     // tampilkan data
    //     echo $jenis_layanan."".$no_antrian22;
    // }
    // else {
    //     // tampilkan "-"
    //     echo "-";
    // }

    // var_dump($no_antrian22);
    // die();

// asli
    // sql statement untuk menampilkan data "no_antrian" dari tabel "queue_antrian_admisi" berdasarkan "tanggal" dan "status = 1"
    $query = mysqli_query($mysqli, "SELECT no_antrian FROM queue_antrian_admisi  WHERE  ( tanggal='$tanggal' AND status='1' and code='$jenis_layanan')  ORDER BY updated_date DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // ambil jumlah baris data hasil query
    $rows = mysqli_num_rows($query);

    // cek hasil query
    // jika data "no_antrian" ada
    if ($rows <> 0) {
        // ambil data hasil query
        $data = mysqli_fetch_assoc($query);
        // buat variabel untuk menampilkan data
        $no_antrian = $data['no_antrian'];

        // tampilkan data
        echo $jenis_layanan."".$no_antrian;
    }
    // jika data "no_antrian" tidak ada
    else {
        // tampilkan "-"
        echo "-";
    }
}
