<?php
// session_start();
// if (empty($_SESSION['username'])) {
//     header('location:../index.php');
// } else {
    include "../../config/database.php";
	require_once "../../config/database.php";
	date_default_timezone_set('Asia/Jakarta');

	// ambil tanggal sekarang
    $tanggal = gmdate("Y-m-d", time() + 60 * 60 * 7);
    // $loket = $_POST['loket'];
    // var_dump($tipe);
    $query = mysqli_query($mysqli, "SELECT * FROM queue_setting ORDER BY id DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // ambil jumlah baris data hasil query
    $rows = mysqli_num_rows($query);

    if ($rows <> 0) {
        $data = mysqli_fetch_assoc($query);
    } else {
        $data = [];
    }

     $list_loket = json_decode($data['list_loket'], true);
     if (count($list_loket) > 0) :
    //  if ($list_loket) !== null) :
         foreach ($list_loket as $lk) : 
            // if($lk['no_loket'] == $loket){
                // echo 
                $jenis_layanan2 = $lk['jenis_layanan'];
            // } else {
                //// echo "0";
            // }
        endforeach; 
     endif;


?>

<!doctype html>
<html lang="en" class="h-100">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi Antrian General Static">
    <meta name="author" content="Ade Rahman">

    <!-- Title -->
    <title>Aplikasi Antrian General Static</title>

    <!-- Favicon icon -->
    <link href="../../assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon">

    <!-- Bootstrap CSS -->
    <link href="../../assets/vendor/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Icons -->
    <link href="../../assets/vendor/css/bootstrap-icons.css" rel="stylesheet">

    <!-- Font -->
    <link href="../../assets/vendor/css/swap.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="../../assets/vendor/css/datatables.min.css" type="text/css" rel="stylesheet">

    <!-- Custom Style -->
    <link href="../../assets/css/style.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
    <main class="flex-shrink-0">
        <div class="container pt-4">
            <div class="d-flex flex-column flex-md-row px-4 py-1 mb-4 bg-white rounded-2 shadow-sm">
                <!-- judul halaman -->
                <div class="d-flex align-items-center me-md-auto">
                    <i class="bi-mic-fill text-success me-3 fs-3"></i>
                    <h1 class="h6 pt-2">Panggilan Antrian <span class="namaLoket"></span></h1>
                </div>
                <!-- breadcrumbs -->
                <div class="ms-5 ms-md-0 pt-md-1 pb-md-0">
                    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/antrianrs/"><i class="bi-house-fill text-success"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Antrian</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <div class="row">
                <!-- menampilkan informasi jumlah antrian -->
                <div class="col-md-3 mb-4">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body p-1">
                            <div class="d-flex justify-content-start">
                                <div class="feature-icon-3 me-4">
                                    <i class="bi-people text-warning"></i>
                                </div>
                                <div>
                                    <p id="jumlah-antrian" class="fs-3 text-warning mb-1"></p>
                                    <p class="mb-0">Jumlah Antrian</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menampilkan informasi nomor antrian yang sedang dipanggil -->
                <div class="col-md-3 mb-4">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body p-0">
                            <div class="d-flex justify-content-start">
                                <div class="feature-icon-3 me-4">
                                    <i class="bi-person-check text-success"></i>
                                </div>
                                <div>
                                    <!-- fs-3 -->
                                    <p id="antrian-sekarang" style="font-size:2rem;" class=" text-success mb-1"></p>
                                    <p class="mb-0">Antrian Sekarang</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menampilkan informasi nomor antrian yang akan dipanggil selanjutnya -->
                <div class="col-md-3 mb-4">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body p-0">
                            <div class="d-flex justify-content-start">
                                <div class="feature-icon-3 me-4">
                                    <i class="bi-person-plus text-info"></i>
                                </div>
                                <div>
                                    <p id="antrian-selanjutnya" class="fs-3 text-info mb-1"></p>
                                    <p class="mb-0">Antrian Selanjutnya</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- menampilkan informasi jumlah antrian yang belum dipanggil -->
                <div class="col-md-3 mb-4">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body p-0">
                            <div class="d-flex justify-content-start">
                                <div class="feature-icon-3 me-4">
                                    <i class="bi-person text-danger"></i>
                                </div>
                                <div>
                                    <p id="sisa-antrian" class="fs-3 text-danger mb-1"></p>
                                    <p class="mb-0">Sisa Antrian</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- menampilkan informasi jumlah antrian -->
                <div class="col-md-3 mb-4">
                    
                </div>
                <!-- menampilkan informasi nomor antrian yang sedang dipanggil -->
                <div class="col-md-3 mb-4">
                    <div class=" border-0 ">
                        <!-- <div class="card-body p-4"> -->
                            <!-- <div class="d-flex justify-content-start"> -->
                                <!-- <div>   -->
                                     <a id="pulang" style="height:70px; width=100%"  href="javascript:void(0)" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-1 mb-2">
                                     Panggil Ulang
                                        </a>
                                <!-- </div> -->
                            <!-- </div> -->
                        <!-- </div> -->
                    </div>
                </div>
                <!-- menampilkan informasi nomor antrian yang akan dipanggil selanjutnya -->
                <div class="col-md-5 mb-4">
                    <div class=" border-0 ">
                        <!-- <div class="card-body p-4"> -->
                            <div class="d-flex justify-content-start">
                                <div>                                 
                                    <a id="pselanjutnya"  href="javascript:void(0)" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                     Panggil Antrian Selanjutnya
                                    </a>
                                    
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
                <!-- menampilkan informasi jumlah antrian yang belum dipanggil -->
                <!-- <div class="col-md-3 mb-4">
                    <div class="border-0 "> -->
                        <!-- <div class="card-body p-4"> -->
                        <!-- <div class="d-flex justify-content-start">
                                <div>                                 
                                    <a id="plewati"  style="height:100px; width=200%" href="javascript:void(0)" class="btn btn-warning btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                     Lewati Antrian
                                    </a>
                                    
                                </div>
                            </div> -->
                        <!-- </div> -->
                    <!-- </div>
                </div> -->
            </div>
            
        </div>
    </main>

    <!-- Footer -->
    <footer class="footer mt-auto py-4">
        <div class="container">
            <hr class="my-4">
            <!-- copyright -->
            <div class="copyright text-center mb-2 mb-md-0">&copy; <?php date('Y') ?> - <a href="https://paperlesshospital.id" target="_blank" class="text-brand text-decoration-none">paperlesshospital.id</a>. All rights reserved.
            </div>
        </div>
    </footer>

    <!-- jQuery Core -->
    <script src="../../assets/vendor/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <!-- Popper and Bootstrap JS -->
    <script src="../../assets/vendor/js/popper.min.js" type="text/javascript"></script>
    <!-- Bootstrap JS -->
    <script src="../../assets/vendor/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- DataTables -->
    <script src="../../assets/vendor/js/datatables.min.js" type="text/javascript"></script>
    <!-- Responsivevoice -->
    
    <script type="text/javascript">
        $(document).ready(function() {
            var loket = localStorage.getItem('_loket');
            $(".namaLoket").html(' Loket ' + loket);
            var jenis_layanan = "";
            // get jenis layanan
            $.ajax({
                    type: 'POST', // mengirim data dengan method POST
                    url: 'getJenisLayanan.php', //.php 
                    data: {loket: loket},
                    success: function(result) { // ketika proses insert data selesai
                        jenis_layanan = result;   
                        
                        // tampilkan informasi antrian
                        $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_jumlah_antrian.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#jumlah-antrian").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

            // $('#antrian-sekarang').load('get_antrian_sekarang.php');
                        $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_antrian_sekarang.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#antrian-sekarang").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                        // $('#antrian-selanjutnya').load('get_antrian_selanjutnya.php');
                        $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_antrian_selanjutnya.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#antrian-selanjutnya").html(result);
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

            // $('#sisa-antrian').load('get_sisa_antrian.php');
                        $.ajax({
                                    type: 'POST', // mengirim data dengan method POST
                                    url: 'get_sisa_antrian.php', // insert.php url file proses insert data
                                    data: {jenis_layanan: jenis_layanan},
                                    success: function(result) { // ketika proses insert data selesai
                                        $("#sisa-antrian").html(result);
                                    }
                                });

                    }
                });

            // tampilkan informasi antrian
            // $('#jumlah-antrian').load('get_jumlah_antrian.php');
            
            // $('#antrian-sekarang').load('get_antrian_sekarang.php');
            // $('#antrian-selanjutnya').load('get_antrian_selanjutnya.php');
            // $('#sisa-antrian').load('get_sisa_antrian.php');

            // menampilkan data antrian menggunakan DataTables
            var table = $('#tabel-antrian').DataTable({
                "lengthChange": false, // non-aktifkan fitur "lengthChange"
                "searching": false, // non-aktifkan fitur "Search"
                // if(jenis_layanan = 'A'){
                    // "ajax": "get_antrianccc.php", // url file proses tampil data dari database
                // } 
                "ajax": "get_antrian.php", // url file proses tampil data dari database
                // menampilkan data
                "columns": [{
                        "data": "code",                        
                        "width": '30px',
                        "orderable": true,
                        "searchable": true,
                        "className": 'text-center',
                        render: function(data) {
                            return '<b>' + data + '</b>'
                        }
                    }
                    ,{
                        "data": "no_antrian",
                        "width": '200px',
                        "orderable": true,
                        "searchable": true,
                        "className": 'text-center',
                        render: function(data) {
                            return '<b>' + data + '</b>'
                        }
                    },
                    {
                        "data": "status",
                        "visible": false
                    },
                    {
                        "data": null,
                        "orderable": false,
                        "searchable": false,
                        "width": '100px',
                        "className": 'text-center',
                        "render": function(data, type, row) {
                            // jika tidak ada data "status"
                            if (data["status"] === "") {
                                // sembunyikan button panggil
                                var btn = "-";
                            }
                            // jika data "status = 0"
                            else if (data["status"] === "0") {
                                // tampilkan button panggil
                                var btn = "<button class=\"btn btn-success btn-sm rounded-circle\"><i class=\"bi-mic-fill\"></i></button>";
                            }
                            // jika data "status = 1"
                            else if (data["status"] === "1") {
                                // tampilkan button ulangi panggilan
                                var btn = "<button class=\"btn btn-secondary btn-sm rounded-circle\"><i class=\"bi-mic-fill\"></i></button>";
                            }
                            // jika data "status = 3"
                            else if (data["status"] === "3") {
                                // tampilkan button ulangi panggilan
                                var btn = "<button class=\"btn btn-success btn-sm rounded-circle\"><i class=\"bi-mic-fill\"></i></button>";
                            };
                            return btn;
                        }
                    },
                ],
                "order": [
                    [0, "desc"] // urutkan data berdasarkan "no_antrian" secara descending
                ],
                "iDisplayLength": 50, // tampilkan 10 data per halaman
            });

            // panggilan antrian dan update data
            $('#tabel-antrian tbody').on('click', 'button', function() {
                // ambil data dari datatables 
                var data = table.row($(this).parents('tr')).data();
                // buat variabel untuk menampilkan data "id"
                var id = data["id"];

                // proses create panggilan antrian
                $.ajax({
                    url: "create_panggilan.php", // url file proses update data
                    type: "POST", // mengirim data dengan method POST
                    // tentukan data yang dikirim
                    dataType: 'json',
                    data: {
                        antrian: data["no_antrian"],
                        loket: loket,
                        code: data["code"]
                    },
                    async: false,
                    cache: false,
                    success: function(data) {
                        console.log(data);
                    }
                });

                // proses update data
                $.ajax({
                    type: "POST", // mengirim data dengan method POST
                    url: "update.php", // url file proses update data
                    // tentukan data yang dikirim
                    data: {
                        id: id
                    }
                });
            });

            // panggil selanjutnya pselanjutnya
            $('#pselanjutnya').on('click', function() {
                var loket = $(".namaLoket").html().slice(7, 8);
                var code = $("#antrian-selanjutnya").html().slice(0,1);;
                var nomor = $("#antrian-selanjutnya").html().slice(1,4);
                               
                if (nomor!="" || code != "-"){
                     // proses create panggilan antrian
                        $.ajax({
                            url: "create_panggilan.php", // url file proses update data
                            type: "POST", // mengirim data dengan method POST
                            // tentukan data yang dikirim
                            dataType: 'json',
                            data: {
                                antrian: nomor,
                                loket: loket,
                                code: code
                            },
                            async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);                    

                                // proses update data
                                    $.ajax({
                                        type: "POST", // mengirim data dengan method POST
                                        url: "update.php", // url file proses update data
                                        // tentukan data yang dikirim
                                        data: {
                                            code: code,
                                            no_antrian: nomor
                                        },
                                        success: function(data) {
                                            console.log(data);
                                        }, error: function (request, error) {
                                                    console.log(arguments);
                                                    alert(" Can't do because: " + error);
                                        },
                                    });
                            },
                                    error: function (request, error) {
                                        console.log(arguments);
                                        alert(" Can't do because: " + error);
                                    },
                        });
                } else {
                     alert("Nomor Antrian Habis");
                }               
                
            });

            // panggil ulang pulang
            $('#pulang').on('click', function() {
                var loket = $(".namaLoket").html().slice(7, 8);
                var code = $("#antrian-sekarang").html().slice(0,1);;
                var nomor = $("#antrian-sekarang").html().slice(1,4);
                               
                if (nomor!="" || code != "-"){
                     // proses create panggilan antrian
                        $.ajax({
                            url: "create_panggilan.php", // url file proses update data
                            type: "POST", // mengirim data dengan method POST
                            // tentukan data yang dikirim
                            dataType: 'json',
                            data: {
                                antrian: nomor,
                                loket: loket,
                                code: code
                            },
                            async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);                    

                                // proses update data
                                    $.ajax({
                                        type: "POST", // mengirim data dengan method POST
                                        url: "update.php", // url file proses update data
                                        // tentukan data yang dikirim
                                        data: {
                                            code: code,
                                            no_antrian: nomor
                                        },
                                        success: function(data) {
                                            console.log(data);
                                        }, error: function (request, error) {
                                                    console.log(arguments);
                                                    alert(" Can't do because: " + error);
                                        },
                                    });
                            },
                                    error: function (request, error) {
                                        console.log(arguments);
                                        alert(" Can't do because: " + error);
                                    },
                        });
                } else {
                     alert("Nomor Antrian Habis");
                }               
                
            });

            // lewati antrian plewati
            $('#plewati').on('click', function() {
                var loket = $(".namaLoket").html().slice(7, 8);
                var code = $("#antrian-sekarang").html().slice(0,1);;
                var nomor = $("#antrian-sekarang").html().slice(1,4);
                               
                if (nomor!="" || code != "-"){
                     // proses create panggilan antrian
                        // proses update data
                        $.ajax({
                            type: "POST", // mengirim data dengan method POST
                            url: "updateLewati.php", // url file proses update data
                            // tentukan data yang dikirim
                            data: {
                                code: code,
                                no_antrian: nomor
                            },
                            success: function(data) {
                                console.log(data);
                            }, error: function (request, error) {
                                        console.log(arguments);
                                        alert(" Can't do because: " + error);
                            },
                        });
                                                    
                }             
                
            });

            // auto reload data antrian setiap 1 detik untuk menampilkan data secara realtime
            setInterval(function() {
                // $('#jumlah-antrian').load('get_jumlah_antrian.php').fadeIn("slow");
                // tampilkan informasi antrian
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_jumlah_antrian.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#jumlah-antrian").html(result).fadeIn("slow");;
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                // $('#antrian-sekarang').load('get_antrian_sekarang.php').fadeIn("slow");
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_antrian_sekarang.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#antrian-sekarang").html(result).fadeIn("slow");
                            },
                            error: function (request, error) {
                                console.log(arguments);
                                alert(" Can't do because: " + error);
                            },
                        });

                // $('#antrian-selanjutnya').load('get_antrian_selanjutnya.php').fadeIn("slow");
                $.ajax({
                            type: 'POST', // mengirim data dengan method POST
                            url: 'get_antrian_selanjutnya.php', // insert.php url file proses insert data
                            data: {jenis_layanan: jenis_layanan},
                            success: function(result) { // ketika proses insert data selesai
                                $("#antrian-selanjutnya").html(result).fadeIn("slow");
                            }
                        });

                // $('#sisa-antrian').load('get_sisa_antrian.php').fadeIn("slow");
                $.ajax({
                                    type: 'POST', // mengirim data dengan method POST
                                    url: 'get_sisa_antrian.php', // insert.php url file proses insert data
                                    data: {jenis_layanan: jenis_layanan},
                                    success: function(result) { // ketika proses insert data selesai
                                        $("#sisa-antrian").html(result).fadeIn("slow");
                                    }
                                });

                table.ajax.reload(null, false);
            }, 1000);
        });
    </script>
</body>

</html>