<?php
// session_start();
// if (empty($_SESSION['username'])) {
//     header('location:../index.php');
// } else {
    include "../../config/database.php";
	require_once "../../config/database.php";
	date_default_timezone_set('Asia/Jakarta');

	// ambil tanggal sekarang
    $tanggal = gmdate("Y-m-d", time() + 60 * 60 * 7);
    $bulan = 5; //$_POST['tipe']
    $tahun = 2024; //$_POST['tipe']
    // var_dump($tipe);
    // membuat "no_antrian"
    // sql statement untuk menampilkan data "no_antrian" terakhir pada tabel "queue_antrian_admisi" berdasarkan "tanggal"
    $query = mysqli_query($mysqli, "SELECT * FROM queue_antrian_admisi WHERE month(tanggal) ='$bulan' and YEAR(tanggal) ='$tahun' ") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // ambil jumlah baris data hasil query
	
    $rows = mysqli_num_rows($query);

    
    // cek hasil query
    if ($rows <> 0) {
        // ambil data hasil query
        // $data = mysqli_fetch_assoc($query);
        $data = mysqli_fetch_all($query);
		// var_dump(count($data));
		// var_dump("======");
		// var_dump($data[0][0]); //422

    }
    // jika "no_antrian" belum ada
    else {
        $no_antrian = sprintf("%03s", 1);
        // $no_antrian =  1;
    }
	$hariIni = new DateTime();

	// function hariIndo($hariInggris) {
    //     switch ($hariInggris) {
    //     case 'Sunday':
    //         return 'Minggu';
    //     case 'Monday':
    //         return 'Senin';
    //     case 'Tuesday':
    //         return 'Selasa';
    //     case 'Wednesday':
    //         return 'Rabu';
    //     case 'Thursday':
    //         return 'Kamis';
    //     case 'Friday':
    //         return 'Jumat';
    //     case 'Saturday':
    //         return 'Sabtu';
    //     default:
    //         return 'hari tidak valid';
    //     }
	// }
?>

<!DOCTYPE html>
<html>
<head>
	
 	  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"  />
 	  <link rel="stylesheet" href="../../assets/css/print_nota_kasir.css" />
	<!-- Latest compiled and minified JavaScript -->
	<script src="../../assets/js/jquery-3.2.1.min.js" ></script>
	<script src="../../assets/js/bootstrap.min.js" ></script>
	<script src="../../assets/js/jquery-ui.min.js"></script>
	<script src="../../assets/js/jQuery.print.min.js"></script>
	<script src="../../assets/js/trumbowyg.min.js"></script>
 	<script src="../../assets/js/id.min.js"></script>

	</head>
	<body>
    <!-- onClick="cetaktagihanall('TX235576')" -->
	<div>
        <!-- <center> <a class="btn btn-success" id="cetaknota" onClick="cetaktagihanall('TX235576')" href="javascript:void(0)" style="height:7cm; width:12cm; font-size: 50px; margin-top:10px; padding:10px;" ><i class="fa fa-print" aria-hidden="true"></i><div style=" margin-top:80px; "> Cetak Nomor</div></a></center> -->
    <!-- <center> <a class="btn btn-danger" id="cetaknota" onClick="closewindow('TX235576')" href="javascript:void(0)" style="height:7cm; width:7cm; font-size: 30px; margin-top:10px; padding:10px;" ><div style=" margin-top:80px; "> CLOSE</div></a></center></div>	 -->
    
	<div id="page-print2"  style="width: 1210;" class="page">
		<div> <br><br></div>

	<div id="content">
 	<div class="row">
        <div class="col-xs-12">
        	<div class="row">
	        	<!-- <div class="invoice-logo">
	        		<img src="../..assets/img/logokab.png" class="img-responsive" alt="Logo Kab">
	        	</div> -->
	    		<div  style="text-align: center;" class="invoice-title">
	    			
	    			<h4 class="text-rs-title" >Data Antrian Bulan <?= $bulan?> Tahun <?=$tahun?></h4>	    			
	                <!-- <p class="text-rs-address"> Jl. Raya Gedeg No.17  ,(0321) 364752</p> -->
					<!-- <h5 class="text-rs-title" style="margin-top: 3px;">NOMOR ANTREAN FARMASI</h5> -->
	    		</div>


                <table class="table table-striped table-hover table-condensed table-sm">
                    <thead>
                        <tr class="bg-primary">
                            <th>No</th>
                            <th>id</th>
                            <th>tanggal</th>
                            <th>code</th>
                            <th>no_antrian</th>
                            <th>status</th>
                            <th>updated_date</th>
                            <th>created_at</th>
                            <th>called_at</th>
                            <!-- <?php
                    if ($kunjungan[0]->status != 2) {
                    ?>
                        <th>Tanggal Keluar</th>
                    <?php
                            } else {
                    ?>
                        <th>Keterangan</th>
                    <?php
                            }
                    ?>
                     -->
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            $x = 1;
                            // foreach ($kunjungan as $k) {
                            // foreach ($data as $k) {
                            for ($i=0; $i < count($data); $i++) { 
                                // for ($hh=0; $hh < count($data[$i]); $hh++) { 
                                
                                    echo '<tr>' .
                                        '<td>' . $x++ . '</td>' .
                                        '<td>' . $data[$i][0] . '</td>' .
                                        '<td>' . $data[$i][1] . '</td>' .
                                        '<td>' . $data[$i][2] . '</td>';

                                    echo '<td>' . $data[$i][3] . '</td>';

                                    // if ($k->cara_bayar == 99) {
                                    //     $cara_bayar_s = "UMUM";
                                    // } else if ($k->cara_bayar == 1) {
                                    //     $cara_bayar_s = "BPJS";
                                    // } 

                                    echo '<td>' . $data[$i][4] . '</td>' .
                                        '<td>' . $data[$i][5] . '</td>' .
                                        '<td>' . $data[$i][6] . '</td>' .
                                        '<td>' . $data[$i][7] . '</td>'; ?>
                                                <?php

                                        echo    '</tr>';
                                    // }
                                }
                                    ?>

                                    </tbody>
                                </table>



    		</div>
    		<hr style="height: 0px;border-top: 1px solid #0e0d0d;margin:4px 26px; ">

			<!-- <h6 class="text-rs-title" style="margin-top: 0px; text-align: center;"><b>ANTRIAN OBAT NON RACIKAN</b></h6> -->

			<!-- <h2 style="text-align: center; font-size: 40px; margin-top: 0px;" class="text-rs-title"><strong> <?php echo $tipe ." ". $no_antrian." " ?> </strong></h2> -->
    		
			<!-- <hr style="height: 0px;border-top: 1px solid #0e0d0d;margin:10px 26px; "> -->

			<!-- <h5 class="text-rs-title" style="margin-top: 0px; text-align: center;" ><?php echo  hariIndo(date('l')) . " " . strftime('%d %B %Y', $hariIni->getTimestamp()) ?></h5> -->
			<!-- <h5 class="" style="margin-top: 0px; text-align: center;" ><?php echo date('H:i:s') ?> WIB</h5> -->
    	</div>
    </div> 

    <div class="row">
        <div class="col-md-12">
            <div class="table-container panel">

                <table class="table table-striped table-hover table-condensed table-sm">
                    
                    <tbody>
                        <?php
            $x = 1;
            foreach ($kunjungan as $k) {

              echo '<tr>' .
                '<td>' . $x++ . '</td>' .
                '<td>' . $k->id . '</td>' .
                '<td>' . $k->no_rm . '</td>' .
                '<td>' . $k->nama . ' ,' . $k->title . '</td>';

              echo '<td>' . $k->tgl_kunjungan . '</td>';

              if ($k->cara_bayar == 99) {
                $cara_bayar_s = "UMUM";
              } else if ($k->cara_bayar == 1) {
                $cara_bayar_s = "BPJS";
              } else if ($k->cara_bayar == 2) {
                $cara_bayar_s = "SPM";
              } else if ($k->cara_bayar == 3) {
                $cara_bayar_s = "BPJS TK";
              } else if ($k->cara_bayar == 4) {
                $cara_bayar_s = "RBH";
              } else if ($k->cara_bayar == 5) {
                $cara_bayar_s = "JASA RAHARJA";
              } else if ($k->cara_bayar == 6) {
                $cara_bayar_s = "JAMPERSAL";
              } else if ($k->cara_bayar == 7) {
                $cara_bayar_s = "LPSK";
              } else if ($k->cara_bayar == 8) {
                $cara_bayar_s = "kEMENKES";
              }

              if ($k->layanan == 1) {
                $layanan_s = "MATA";
              } else if ($k->layanan == 3) {
                $layanan_s = "GIGI";
              } else if ($k->layanan == 4) {
                $layanan_s = "ANAK";
              } else if ($k->layanan == 5) {
                $layanan_s = "DALAM";
              } else if ($k->layanan == 6) {
                $layanan_s = "FISIOTERAPI";
              } else if ($k->layanan == 7) {
                $layanan_s = "KULIT";
              } else if ($k->layanan == 8) {
                $layanan_s = "DOT";
              } else if ($k->layanan == 9) {
                $layanan_s = "OBGYN";
              } else if ($k->layanan == 10) {
                $layanan_s = "SYARAF";
              } else if ($k->layanan == 11) {
                $layanan_s = "BEDAH";
              } else if ($k->layanan == 12) {
                $layanan_s = "THT";
              } else if ($k->layanan == 13) {
                $layanan_s = "JIWA";
              } else if ($k->layanan == 14) {
                $layanan_s = "ORTO";
              } else if ($k->layanan == 15) {
                $layanan_s = "PARU";
              } else if ($k->layanan == 16) {
                $layanan_s = "UGD";
              } else if ($k->layanan == 26) {
                $layanan_s = "JANTUNG";
              } else if ($k->layanan == 28) {
                $layanan_s = "UMUM";
              } else if ($k->layanan == 36) {
                $layanan_s = "KAKTUS";
              } else if ($k->layanan == 44) {
                $layanan_s = "E2";
              } else if ($k->layanan == 121) {
                $layanan_s = "Bedah Anak";
              } else if ($k->layanan == 120) {
                $layanan_s = "GIGI PERIODONTI";
              } else if ($k->layanan == 116) {
                $layanan_s = "GIGI PRSOSTHODONTI";
              } else if ($k->layanan == 114) {
                $layanan_s = "GIGI ENDODONSI";
              }

              if ($k->status_keluar == 1) {
                $status_keluar_s = "Diproses";
              } else if ($k->status_keluar == 2) {
                $status_keluar_s = "Pulang";
              } else if ($k->status_keluar == 3) {
                $status_keluar_s = "Rujuk Rawat Inap";
              } else if ($k->status_keluar == 4) {
                $status_keluar_s = "Kabur";
              } else if ($k->status_keluar == 6) {
                $status_keluar_s = "Ke Rumah Sakit Lain";
              } else if ($k->status_keluar == 7) {
                $status_keluar_s = "Meninggal";
              } else if ($k->status_keluar == 8) {
                $status_keluar_s = "Pulang Paksa";
              } else if ($k->status_keluar == 9) {
                $status_keluar_s = "Rujuk Poli Lain";
              }

              echo '<td>' . $cara_bayar_s . '</td>' .
                '<td>' . $layanan_s . '</td>' .
                '<td>' . $k->asal_poli . '</td>' .
                '<td>' . $status_keluar_s . '</td>' .
                '<td>' . $k->status . '</td>'; ?>
                        <td width="160px" class="text-center">
                            <a href="<?= site_url('master_tools/hapus_kunjungan_aksi/' . $k->id.'/'.$k->no_rm) ?>"
                                class="btn btn-danger btn-xs" onClick="return confirm('Konfirmasi Hapus Data ?');">
                                <i class="fa fa-trash"></i> Hapus Kunjungan
                            </a><br>
                            <a href="<?= site_url('master_tools/ubah_tanggal/' . $k->id) ?>"
                                class="btn btn-warning btn">
                                <i class="fa fa-trash"></i> Ubah Tanggal Kunjungan
                            </a>
                            <a href="<?= site_url('master_tools/kunjungan_batal/' . $k->id) ?>"
                                class="btn btn-info btn">
                                <i class="fa fa-trash"></i> Update Kunjungan Batal
                            </a>

                        </td> <?php

                    //   $status = '';
                    // if ($this->session->userdata['poli'] != 0) {
                    //   $status = '<a href="' . base_url() . 'layanan_ranap/diagnosa_ranap/' . $k->id . '" class="btn btn-default"  >Lihat</a>';
                    // } else {
                    //   if ($k->status == 0) {
                    //     $status = 'Pasien Ditolak';
                    //   } else if ($k->status == 1) {
                    //     $status = '<a href="' . base_url() . 'layanan_ranap/submit_kamar/' . $k->id . '" class="btn btn-default"  >Proses</a> <a class="btn btn-default"><i class="fa fa-times" aria-hidden="true"></i></a>';
                    //   } else if ($k->status == 4) {

                    //     $status = 'Pasien telah dipindah kamar';
                    //   } else {

                    //     $status = '<a href="' . base_url() . 'layanan_ranap/pindah_kamar/' . $k->id . '" class="btn btn-default"  >Pindah Kamar</a> ';
                    //   }
                    // }
                    // echo '<td>' . $status . '</td>' .
                    '</tr>';
                  }
                    ?>

                    </tbody>
                </table>
            </div>

        </div>
    </div>

    

</div>
<script src="../../assets/js/custom.js" ></script>

<script type="text/javascript">
        $(document).ready(function() {
        //   window.print();
        //   window.print();

          function cetaknota(val) {
            }
            // window.close(); //BUKA
        }); 
</script>
</body>
</html>