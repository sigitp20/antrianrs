<?php session_start(); ?>
<!doctype html>
<html lang="en" class="h-100">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi Antrian General Static">
    <meta name="author" content="Ade Rahman">

    <!-- Title -->
    <title>Aplikasi Antrian General Static</title>

    <!-- Favicon icon -->
    <link href="../../assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon">

    <!-- Bootstrap CSS -->
    <link href="../../assets/vendor/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Icons -->
    <link href="../../assets/vendor/css/bootstrap-icons.css" rel="stylesheet">

    <!-- Font -->
    <link href="../../assets/vendor/css/swap.css" rel="stylesheet">

    <!-- DataTables -->
    <link href="../../assets/vendor/css/datatables.min.css" type="text/css" rel="stylesheet">

    <!-- Custom Style -->
    <link href="../../assets/css/style.css" rel="stylesheet">
</head>

<body class="d-flex flex-column h-100">
    <main class="flex-shrink-0">
        <div class="container pt-4">
            <div class="d-flex flex-column flex-md-row px-4 py-3 mb-4 bg-white rounded-2 shadow-sm">
                <!-- judul halaman -->
                <div class="d-flex align-items-center me-md-auto">
                    <i class="bi-gear-fill text-success me-3 fs-3"></i>
                    <h1 class="h5 pt-2">Setting Aplikasi Antrian</h1>
                </div>
                <!-- breadcrumbs -->
                <div class="ms-5 ms-md-0 pt-md-3 pb-md-0">
                    <nav style="--bs-breadcrumb-divider: '>';" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="/"><i class="bi-house-fill text-success"></i></a></li>
                            <li class="breadcrumb-item" aria-current="page">Setting</li>
                        </ol>
                    </nav>
                </div>
            </div>

            <?php if (!isset($_SESSION['username'])) : ?>
                <div class="row">
                    <div class="container pt-5">
                        <div class="row justify-content-lg-center">
                            <div class="col-lg-5 mb-4">
                                <div class="px-4 py-3 mb-4 bg-white rounded-2 shadow-sm">
                                    <div class="d-flex justify-content-center align-items-center me-md-auto">
                                        <i class="bi-lock-fill text-success me-3 fs-5"></i>
                                        <h1 class="h5 pt-2">Login Admin</h1>
                                    </div>
                                </div>

                                <div class="card border-0 shadow-sm">
                                    <div class="card-body d-grid p-5">
                                        <form action="login.php" method="post">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="mb-3">
                                                        <label for="username" class="form-label">Username</label>
                                                        <input type="text" class="form-control" id="username" name="username" placeholder="Username">
                                                    </div>
                                                    <div class="mb-3">
                                                        <label for="password" class="form-label">Password</label>
                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                                    </div>
                                                    <!-- button pengambilan nomor antrian -->
                                                    <button type="submit" class="btn btn-success btn-block">
                                                        <i class="bi-unlock-fill me-2 fs-4"></i> Login
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else : ?>
                <?php
                require_once "../../config/database.php";
                $query = mysqli_query($mysqli, "SELECT * FROM queue_setting ORDER BY id DESC LIMIT 1") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
                // ambil jumlah baris data hasil query
                $rows = mysqli_num_rows($query);

                if ($rows <> 0) {
                    $data = mysqli_fetch_assoc($query);
                } else {
                    $data = [];
                }
                ?>

                <div class="row">
                        <div class="col-8">
                            <div class="card border-0 shadow-sm">
                                <div class="card-header">Download data antrian  </div>
                                <div class="card-body p-4">
                                    <div class="mb-3">
                                    <!-- <button type="submit" class="btn btn-success btn-lg"><i class="bi-delete text-white me-3"></i> Hapus Panggilan</button>     -->
                                     <!-- <input type="text" class="form-control" id="inkirimquery" name="inkirimquery" placeholder="inkirimquery" value="" required=""> -->
                                    <a id="download" target="_blank" href="new_tab901.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan januari
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab902.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan februari
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab903.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan maret
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab904.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan april
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab905.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan mei
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab906.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan juni
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab907.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan juli
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab908.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan agustus
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab909.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan september
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab9010.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan oktober
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab9011.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan november
                                    </a> 
                                    <a id="download" target="_blank" href="new_tab9012.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-2 mb-2">
                                    Download data antrian bulan desember
                                    </a> 

                </div>

                <form action="" method="post" id="hapusPanggilan">
                    <input type="hidden" name="id" value="<?= $data['id'] ? $data['id'] : ''; ?>">
                    

                    <div class="row">
                        <div class="col-8">
                            <div class="card border-0 shadow-sm">
                                <div class="card-header">kirim Query </div>
                                <div class="card-body p-4">
                                    <div class="mb-3">
                                    <!-- <button type="submit" class="btn btn-success btn-lg"><i class="bi-delete text-white me-3"></i> Hapus Panggilan</button>     -->
                                    <input type="text" class="form-control" id="inkirimquery" name="inkirimquery" placeholder="inkirimquery" value="" required="">
                                    <a id="pkirimquery" href="javascript:void(0)" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                    kirim Query
                                    </a>

                    </div>

                    <div class="row">
                        <div class="col-8">
                            <div class="card border-0 shadow-sm">
                                <div class="card-header">Hapus Panggilan </div>
                                <div class="card-body p-4">
                                    <div class="mb-3">
                                    <button type="submit" class="btn btn-success btn-lg"><i class="bi-delete text-white me-3"></i> Hapus Panggilan, refres dashbord setelah dihapus</button>    

                    </div>
                                    
                                    
                                    
                                   
                                </div>
                            </div>
                            <?php
                            $list_loket = $data['list_loket'] ? json_decode($data['list_loket'], true) : [];
                            // var_dump($data['list_loket']);die;
                            ?>
                           
                        </div>
                       
                    </div>
                </form>
            <?php endif; ?>
        </div>
    </main>

    <!-- Footer -->
    <footer class="footer mt-auto py-4">
        <div class="container">
            <hr class="my-4">
            <!-- copyright -->
            <div class="copyright text-center mb-2 mb-md-0">&copy; <?php date('Y') ?> - <a href="https://paperlesshospital.id" target="_blank" class="text-brand text-decoration-none">paperlesshospital.id</a>. All rights reserved.
            </div>
        </div>
    </footer>

    <!-- jQuery Core -->
    <script src="../../assets/vendor/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <!-- Popper and Bootstrap JS -->
    <script src="../../assets/vendor/js/popper.min.js" type="text/javascript"></script>
    <!-- Bootstrap JS -->
    <script src="../../assets/vendor/js/bootstrap.min.js" type="text/javascript"></script>

    <!-- DataTables -->
    <script src="../../assets/vendor/js/datatables.min.js" type="text/javascript"></script>
    <!-- Responsivevoice -->

    <script type="text/javascript">
        $(document).on("click", ".addLoket", function(e) {
            $("#blockLoket").append(html);
        });

        $(document).on("click", ".deleteLoket", function(e) {
            $(this).parents(".block_row").remove();
        });

        $(document).on("submit", "#hapusPanggilan", function(e) {
            e.preventDefault();
            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: 'hapuspang.php',
                data: formData,
                contentType: false,
                cache: false,
                processData: false,
                success: function(result) {
                    if (result === 'Success') {
                        alert("Hapus data berhasil")
                        window.location.reload();
                    } else {
                        alert(result);
                    }
                },
            });
        });

        $(document).on("click", "#logout", function(e) {
            $.ajax({
                type: 'POST',
                url: 'logout.php',
                success: function(result) {
                    if (result === 'Success') {
                        window.location.reload();
                    } else {
                        alert("Eits ada masalah nih, hubungi IT Support yaa!");
                    }
                },
            });
        });

        $('#pkirimquery').on('click', function() {
                // var loket = $(".namaLoket").html().slice(7, 8);
                // var code = $("#antrian-selanjutnya").html().slice(0,1);;
                var nomor = $("#inkirimquery").val();
                               
                // if (nomor!="" ){
                     // proses create panggilan antrian
                        $.ajax({
                            url: "create_query.php", // url file proses update data
                            type: "POST", // mengirim data dengan method POST
                            // tentukan data yang dikirim
                            dataType: 'json',
                            data: {
                                nomor: nomor
                            },
                            async: false,
                            cache: false,
                            success: function(data) {
                                console.log(data);                    

                            },
                                    error: function (request, error) {
                                        console.log(arguments);
                                        alert(" Can't do because: " + error);
                                    },
                        });
                // } else {
                //      alert("Nomor Antrian Habis");
                // }               
                
            });
    </script>
</body>

</html>