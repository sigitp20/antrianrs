<!doctype html>
<html lang="en" class="h-100">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Aplikasi Antrian General Static">
    <meta name="author" content="Ade Rahman">

    <!-- Title -->
    <title>Aplikasi Antrian General Static</title>

    <!-- Favicon icon -->
    <link href="../../assets/img/favicon.ico" type="image/x-icon" rel="shortcut icon">

    <!-- Bootstrap CSS -->
    <link href="../../assets/vendor/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Icons -->
    <link href="../../assets/vendor/css/bootstrap-icons.css" rel="stylesheet">

    <!-- Font -->
    <link href="../../assets/vendor/css/swap.css" rel="stylesheet">

    <!-- Custom Style -->
    <link rel="stylesheet" href="../../assets/css/style.css">

    <link rel="stylesheet" href="../../assets/css/trumbowyg.min.css"/>
	<link rel="stylesheet" href="../../assets/css/bootstrap.min.css"  />
	<link rel="stylesheet" href="../../assets/css/bootstrap-theme.min.css" />
	<link rel="stylesheet" href="../../assets/css/style.css" type="text/css"  />
	<link rel="stylesheet" href="../../assets/css/jquery-ui.css">
	<link rel="stylesheet" href="../../assets/css/font-awesome.min.css">
	<link rel="stylesheet" href="../../assets/css/tableexport.min.css">
	<link rel="stylesheet" href="../../assets/css/jquery.datetimepicker.min.css">
	<link rel="stylesheet" href="../../assets/css/custom.css">
	

	<!-- Latest compiled and minified JavaScript -->
	<script src="../../assets/js/jquery-3.2.1.min.js" ></script>
	<script src="../../assets/js/bootstrap.min.js" ></script>
	<script src="../../assets/js/jquery-ui.min.js"></script>
	<script src="../../assets/js/jQuery.print.min.js"></script>
	<script src="../../assets/js/jquery.datetimepicker.full.min.js"></script>

	<!-- trumbowyg Plugin -->
 	
 	<script src="../../assets/js/trumbowyg.min.js"></script>
 	<script src="../../assets/js/id.min.js"></script>


	<!-- export xls plugin -->
	<script src="../../assets/js/xlsx.core.min.js"></script>
	<script src="../../assets/js/FileSaver.min.js"></script>
	<script src="../../assets/js/tableexport.min.js"></script>

	<!-- TABLE PLUGIN -->
	<link rel="stylesheet" type="text/css" href="../../assets/css/datatables.min.css"/>
 	<script type="text/javascript" src="../../assets/js/datatables.min.js"></script>

 	<!-- CONTEXT MENU PLUGIN -->
 	<link rel="stylesheet" type="text/css" href="../../assets/css/jquery.contextMenu.min.css"/>
 	<script type="text/javascript" src="../../assets/js/jquery.ui.position.min.js"></script>
 	<script type="text/javascript" src="../../assets/js/jquery.contextMenu.min.js"></script>

</head>

<body class="d-flex flex-column h-100">
    <main class="flex-shrink-0">
        <div class="container pt-5">
            <div class="row justify-content-lg-center">
            <div class="px-4 py-3 mb-4 bg-white rounded-2 shadow-sm">
                        <!-- judul halaman -->
                        <div class="d-flex align-items-center me-md-auto">
                            <i class="bi-people-fill text-success me-3 fs-3"></i>
                            <h1 class="h5 pt-2">Nomor Antrian Farmasi</h1>
                        </div>
                    </div>
                <!-- menu 1 A -->
            <div class="col-lg-4 mb-4">      
                    <div class="card border-0 shadow-sm">
                        <div class="card-body text-center d-grid p-5">
                            <div class="border border-success rounded-2 py-2 mb-5">
                                <h3 class="pt-4">ANTRIAN OBAT NON RACIKAN</h3>
                                <!-- menampilkan informasi jumlah antrian -->
                                <h1 id="antrian" class="display-1 fw-bold text-success text-center lh-1 pb-2"></h1>
                            </div>
                            <!-- button pengambilan nomor antrian -->
                            
                                <!-- target="_blank" -->
                                <!--  href="javascript:void(0)" -->
                            <a id="insert" target="_blank" href="new_tab.php"  class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                <i class="bi-person-plus fs-4 me-2"></i> Ambil Nomor
                            </a>

                            <!-- <button id="insert" href="javascript:void(0)" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                <i class="bi-person-plus fs-4 me-2"></i> Ambil Nomor
                            </button> -->


                        </div>
                    </div>
                </div>

                <!-- menu 2 B -->
                <div class="col-lg-4 mb-4">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body text-center d-grid p-5">
                            <div class="border border-success rounded-2 py-2 mb-5">
                                <h3 class="pt-4">ANTRIAN OBAT RACIKAN</h3>
                                <!-- menampilkan informasi jumlah antrian -->
                                <h1 id="antrian22" class="display-1 fw-bold text-success text-center lh-1 pb-2"></h1>
                            </div>
                            <!-- button pengambilan nomor antrian -->
                            <a id="insert22" target="_blank" href="new_tabbb.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                <i class="bi-person-plus fs-4 me-2"></i> Ambil Nomor
                            </a>
                        </div>
                    </div>
                </div>

                <!-- menu 3 c -->
                <div class="col-lg-4 mb-4">

                    <div class="card border-0 shadow-sm">
                        <div class="card-body text-center d-grid p-5">
                            <div class="border border-success rounded-2 py-2 mb-5">
                                <h3 class="pt-4">ANTRIAN OBAT </h3>
                                <!-- menampilkan informasi jumlah antrian -->
                                 <h1 id="antrian333" class="display-1 fw-bold text-success text-center lh-1 pb-2"></h1>
                            </div> 
                            <!-- button pengambilan nomor antrian -->
                            <a id="insert333" target="_blank" href="new_tabccc.php" class="btn btn-success btn-block rounded-pill fs-5 px-5 py-4 mb-2">
                                <i class="bi-person-plus fs-4 me-2"></i> Ambil Nomor
                            </a>
                        </div>
                    </div>
                </div>

            </div>

            <!-- style="display: none;" -->
            <div id="page-print" style="display: none;">
                <div class="container">
                    <!-- copyright -->
                    <div class="copyright text-center mb-2 mb-md-0">&copy; <?php date('Y') ?> - <a href="https://paperlesshospital.id" target="_blank" class="text-brand text-decoration-none">paperlesshospital.id</a>. All rights reserved.
                    </div>
                </div>
            </div>
            
        </div>

        

        
    </main>

    <!-- Footer -->
    <footer class="footer mt-auto py-4">
        <div class="container">
            <!-- copyright -->
            <div class="copyright text-center mb-2 mb-md-0">&copy; <?php date('Y') ?> - <a href="https://paperlesshospital.id" target="_blank" class="text-brand text-decoration-none">paperlesshospital.id</a>. All rights reserved.
            </div>
        </div>
    </footer>

    <!-- jQuery Core -->
    <script src="../../assets/vendor/js/jquery-3.6.0.min.js" type="text/javascript"></script>
    <!-- Popper and Bootstrap JS -->
    <script src="../../assets/vendor/js/popper.min.js" type="text/javascript"></script>
    <!-- Bootstrap JS -->
    <script src="../../assets/vendor/js/bootstrap.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#page-print").hide();

            // tampilkan jumlah antrian
            $('#antrian').load('get_antrian.php');
            $('#antrian22').load('get_antrian_b.php');
            $('#antrian333').load('get_antrian_c.php');


            // proses insert data A
            $('#insert').on('click', function() {
                $.ajax({
                    type: 'POST', // mengirim data dengan method POST
                    url: 'insert.php', // url file proses insert data
                    data: {tipe: "A"},
                    success: function(result) { // ketika proses insert data selesai
                        // jika berhasil
                        console.log(result);
                        if (result === 'Sukses') {
                            // tampilkan jumlah antrian
                            $.get("get_antrian.php", function(data, status) {
                                $('#antrian').html(data).fadeIn('slow');
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                            });
                        } else if (result.includes('Sukses')) {
                            $.get("get_antrian.php", function(data, status) {
                                $('#antrian').html(data).fadeIn('slow');
                                // alert("Antrian anda " + data + " berhasil di ambil, tapi printer bermasalah!");
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                            });
                        } else {
                            alert("Eits ada masalah nih, hubungi IT Support yaa!");
                        }
                    },
                    error: function (request, error) {
                        console.log(arguments);
                        alert(" Can't do because: " + error);
                    },
                });
            });

            // proses insert data B
            $('#insert22').on('click', function() {
                $.ajax({
                    type: 'POST', // mengirim data dengan method POST
                    url: 'insert.php', // url file proses insert data
                    data: {tipe: "B"},
                    success: function(result) { // ketika proses insert data selesai
                        // jika berhasil
                        if (result === 'Sukses') {
                            // tampilkan jumlah antrian
                            $.get("get_antrian_b.php", function(data, status) {
                                $('#antrian22').html(data).fadeIn('slow');
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                                
                            });
                        } else if (result.includes('Sukses')) {
                            $.get("get_antrian_b.php", function(data, status) {
                                $('#antrian22').html(data).fadeIn('slow');
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                               
                                // alert("Antrian anda " + data + " berhasil di ambil, tapi printer bermasalah!");
                            });
                        } else {
                            alert("Eits ada masalah nih, hubungi IT Support yaa!");
                        }
                    },
                });
            });

            // proses insert data C
            $('#insert333').on('click', function() {
                $.ajax({
                    type: 'POST', // mengirim data dengan method POST
                    url: 'insert.php', // url file proses insert data
                    data: {tipe: "C"},
                    success: function(result) { // ketika proses insert data selesai
                        // jika berhasil
                        if (result === 'Sukses') {
                            // tampilkan jumlah antrian
                            $.get("get_antrian_c.php", function(data, status) {
                                $('#antrian333').html(data).fadeIn('slow');
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                                
                            });
                        } else if (result.includes('Sukses')) {
                            $.get("get_antrian_c.php", function(data, status) {
                                $('#antrian333').html(data).fadeIn('slow');
                                $("#page-print").html(data);
                                console.log(data);
                                // cetaknota(data);
                               
                                // alert("Antrian anda " + data + " berhasil di ambil, tapi printer bermasalah!");
                            });
                        } else {
                            alert("Eits ada masalah nih, hubungi IT Support yaa!");
                        }
                    },
                });
            });
        });


        function cetaknota(val) {
            // var no_invoice = pad(val, 6);
            var no_invoice = val
            // $.get(url + "layanan_kasir/get_invoice/" + val, function(data) {
            // $.get("cetak22.php", function(data) {
                // $("#page-print").html(data);
                $("#page-print").show();

                //  $("#page-print").print({
                //      mediaPrint: true,
                //      title: "Nota Bill " + no_invoice
                //  });

                // $('#page-print').print();
                // console.log($('#page-print'));
                // window.print();

                $("#page-print").html("");
                $("#page-print").hide();

                //alert( "Load was performed." );
            // }); //end get

            
        }
    </script>
</body>

</html>