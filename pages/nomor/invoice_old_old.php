 <?php date_default_timezone_set('Asia/Jakarta');

?>
<div id="print-invoice" class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h3 class="text-rs-title"><?php echo "";?></h3>
                <p class="text-rs-address"> <?php echo ""?></p>
    		</div>
    		<hr class="invoice-hr">
    		<div class="row">
    			<div class="col-xs-7">
    				
    			</div>
    			<div class="col-xs-5 ">
    				<address>
        			<strong>Tagihan Kepada:</strong><br>
    					<table  id="nota-atas" class="table table-hover table-condensed table-billing table-sm nota">
     <tbody>
       
        <tr>
            <td class="col-md-1">Nama </td>
            <td class="col-md-4"> <?php echo "".' ,'."";?> </td>
        </tr>
        <tr>
          <td  >No RM</td>
          <td ><?php echo ""?></td>
        </tr>
      
        <tr>
              <td>22 </td>
              <td> <?php
                
                 echo "";?> </td>
        </tr>
        <tr>
              <td>Unit </td>
              <td> <?php echo ""?> </td>
        </tr>
        <tr>
              <td>Pembayaran </td>
              <td> <?php echo ""?> </td>
        </tr>





     </tbody>
     </table>
    			</div>
    		</div>
    		
    	</div>
    </div> 


    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title">No Tagihan :<strong><?php echo sprintf('%06d', "");?></strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table id="nota" class="table table-condensed table-sm invoice">
    						<thead>
                                <tr style="border-bottom: 1px solid;">
        							<td><strong>Nama Jasa</strong></td>
        							
        							<td class="text-center"><strong>Tarif</strong></td>
                                    <td class="text-center"><strong>qty</strong></td>
        							<td class="text-center"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>

                             <?php 
                              $total=0;
                              $total_tagihan=0;
                              $idkunjungan=0;
                              $total_sudah_dibayar=0;
                            //   foreach($pembayaran as $k){
                            //    $idkunjungan=$k->id_trans_rajal;
                            //     if($k->status_bayar==0)
                            //       $total+=($k->harga*$k->qty);
                            //     else
                            //          $total_sudah_dibayar+=($k->harga*$k->qty);
                            //    $total_tagihan+=($k->harga*$k->qty);
                                echo '<tr>'.
                                '<td>'."".'</td>';
                                
                                
                                
                                echo '<td >Rp. '."".'</td>';
                                echo '<td class="text-center">'."".'</td>';
                                echo '<td class="text-right">Rp. '."".'</td>';
                                echo '</tr>';

                                
                            //   }
                              ?>
          <tr style="border-top:1px; border-top-style: solid;">
             <td colspan="3" class="text-note">Total biaya</td>
             <td class="text-right"><strong>Rp. <?php echo ""; ?> </strong></td>

         </tr>  
         <tr >
             <td colspan="3" class="text-note">Total Biaya yang sudah dibayar</td>
             <td class="text-right"><strong>Rp. <?php ""; ?> </strong></td>

         </tr>
         <tr>
            <td colspan="3" class="text-note">Total yang harus dibayar</td>
            <td class="text-right"><strong>Rp. <?php ""; ?> </strong></td>

         </tr>

    							
    						</tbody>
                  <!-- Catatan -->
                <tfoot>
                  <tr><td colspan="4">&nbsp</td></tr>
                  <tr><td colspan="3">Catatan</td><td class="text-center">Kasir</td></tr>
                  <tr ><td colspan="3">Dicetak pada <?php echo date('d/m/Y h:i:s');?></td><td class="text-center"></td></tr>
                  <tr ><td colspan="3"></td><td class="text-center">(<?php echo "";?>)</td></tr>
                </tfoot>
    					</table>
    				</div>
                  
    			</div>
    		</div>
    	</div>
    </div>
</div> 