<?php
// session_start();
// if (empty($_SESSION['username'])) {
//     header('location:../index.php');
// } else {
    include "../../config/database.php";
	require_once "../../config/database.php";
	date_default_timezone_set('Asia/Jakarta');

	// ambil tanggal sekarang
    $tanggal = gmdate("Y-m-d", time() + 60 * 60 * 7);
    $tipe = "B"; //$_POST['tipe']
    // var_dump($tipe);
    // membuat "no_antrian"
    // sql statement untuk menampilkan data "no_antrian" terakhir pada tabel "queue_antrian_admisi" berdasarkan "tanggal"
    $query = mysqli_query($mysqli, "SELECT max(no_antrian) as nomor FROM queue_antrian_admisi WHERE code='$tipe' and tanggal='$tanggal'") or die('Ada kesalahan pada query tampil data : ' . mysqli_error($mysqli));
    // ambil jumlah baris data hasil query
	
    $rows = mysqli_num_rows($query);

    // cek hasil query
    // jika "no_antrian" sudah ada
    if ($rows <> 0) {
        // ambil data hasil query
        $data = mysqli_fetch_assoc($query);
        // "no_antrian" = "no_antrian" yang terakhir + 1
        // $no_antrian = $tipe."".sprintf("%03s", (int)$data['nomor'] + 1);
		if ($data['nomor'] == null || $data['nomor'] == 0){
            $no_antrian = sprintf("%03s", (int)$data['nomor'] + 1);  //+ 1
        } else {
            $no_antrian = sprintf("%03s", (int)$data['nomor']);
        }
        // $no_antrian =  (int)$data['nomor'] ; //+ 1
        // var_dump($no_antrian);
    }
    // jika "no_antrian" belum ada
    else {
        // "no_antrian" = 1
        $no_antrian = sprintf("%03s", 1);
        // $no_antrian =  1;
    }
	$hariIni = new DateTime();

	function hariIndo($hariInggris) {
        switch ($hariInggris) {
        case 'Sunday':
            return 'Minggu';
        case 'Monday':
            return 'Senin';
        case 'Tuesday':
            return 'Selasa';
        case 'Wednesday':
            return 'Rabu';
        case 'Thursday':
            return 'Kamis';
        case 'Friday':
            return 'Jumat';
        case 'Saturday':
            return 'Sabtu';
        default:
            return 'hari tidak valid';
        }
	}
?>

<!DOCTYPE html>
<html>
<head>
	
 	  <link rel="stylesheet" href="../../assets/css/bootstrap.min.css"  />
 	  <link rel="stylesheet" href="../../assets/css/print_nota_kasir.css" />
	<!-- Latest compiled and minified JavaScript -->
	<script src="../../assets/js/jquery-3.2.1.min.js" ></script>
	<script src="../../assets/js/bootstrap.min.js" ></script>
	<script src="../../assets/js/jquery-ui.min.js"></script>
	<script src="../../assets/js/jQuery.print.min.js"></script>
	<script src="../../assets/js/trumbowyg.min.js"></script>
 	<script src="../../assets/js/id.min.js"></script>

	</head>
	<body>
    <!-- onClick="cetaktagihanall('TX235576')" -->
		<center> <a class="btn btn-success" id="cetaknota" onClick="cetaktagihanall('TX235576')" href="javascript:void(0)" style="height:7cm; width:12cm; font-size: 50px; margin-top:10px; padding:10px;" ><i class="fa fa-print" aria-hidden="true"></i><div style=" margin-top:80px; "> Cetak Nomor</div></a></center>
        <center> <a class="btn btn-danger" id="" onClick="closewindow('TX235576')" href="javascript:void(0)" style="height:7cm; width:7cm; font-size: 30px; margin-top:10px; padding:10px;" ><div style=" margin-top:80px; "> CLOSE</div></a></center></div>	
	<div id="page-print" class="page">
		<div> <br><br></div>

	<div id="content">
 	<div class="row">
        <div class="col-xs-12">
        	<div class="row">
	        	<!-- <div class="invoice-logo">
	        		<img src="../..assets/img/logokab.png" class="img-responsive" alt="Logo Kab">
	        	</div> -->
	    		<div  style="text-align: center;" class="invoice-title">
	    			
	    			<h4 class="text-rs-title" >RSUD R.A. BASOENI</h4>	    			
	                <!-- <p class="text-rs-address"> Jl. Raya Gedeg No.17  ,(0321) 364752</p> -->
					<h5 class="text-rs-title" style="margin-top: 3px;">NOMOR ANTREAN FARMASI</h5>
	    		</div>
    		</div>

    		<hr style="height: 0px;border-top: 1px solid #0e0d0d;margin:4px 26px; ">
			<h6 class="text-rs-title" style="margin-top: 0px; text-align: center;"><b>ANTRIAN OBAT RACIKAN</b></h6>

			<h2 style="text-align: center; font-size: 40px; margin-top: 0px;" class="text-rs-title"><strong> <?php echo $tipe ." ". $no_antrian." " ?> </strong></h2>
    		
			<hr style="height: 0px;border-top: 1px solid #0e0d0d;margin:10px 26px; ">

			<h5 class="text-rs-title" style="margin-top: 0px; text-align: center;" ><?php echo  hariIndo(date('l')) . " " . strftime('%d %B %Y', $hariIni->getTimestamp()) ?></h5>
			<h5 class="" style="margin-top: 0px; text-align: center;" ><?php echo date('H:i:s') ?> WIB</h5>
    	</div>
    </div> 

    

</div>
<script src="../../assets/js/custom.js" ></script>

<script type="text/javascript">
        $(document).ready(function() {
        //   window.print();
        //   window.print();

            // window.close();
        }); 
</script>
</body>
</html>